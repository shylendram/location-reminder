package com.geeklabs.lreminder.service;

import android.app.IntentService;
import android.content.Intent;

import com.geeklabs.locationringer.domain.Track;
import com.geeklabs.locationringer.util.NetworkService;
import com.geeklabs.lreminder.communication.AbstractHttpPostTask;
import com.geeklabs.lreminder.communication.post.task.SendMyLocationTask;

public class SendTrackService extends IntentService {

	public SendTrackService() {
		super("Send tracks to server");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// Send request to server if n/w available
		Track track = (Track) intent.getSerializableExtra("com.geeklabs.locationringer.track");
		if (NetworkService.isNetWorkAvailable(getApplicationContext())) {
			AbstractHttpPostTask sendTrackRequest = new SendMyLocationTask(getApplicationContext(), null, track);
			sendTrackRequest.execute();
		}
	}
}