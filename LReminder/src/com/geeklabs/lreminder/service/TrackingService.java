package com.geeklabs.lreminder.service;

import android.app.Service;
import android.content.ComponentCallbacks;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.geeklabs.locationringer.domain.Track;
import com.geeklabs.locationringer.util.LocationServices;
import com.geeklabs.locationringer.util.TrackingServiceManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;

public class TrackingService extends Service implements LocationListener, ComponentCallbacks, OnConnectionFailedListener {
	private static final String LOG_TAG = "TrackingService";
	// A request to connect to Location Services
	private LocationRequest mLocationRequest;
	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;

	private Location location;

	private double currentLat;
	private double currentLng;

	private double lastTrackedLat;
	private double lastTrackedLng;
	private Track track;
	private final IBinder mBinder = new UserLocationBinder();

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (!TrackingServiceManager.canContinue(getApplicationContext())) {
			Log.w(LOG_TAG, "Trying to start service, but no user logged in the app");
			return Service.START_NOT_STICKY; // Don't do anything as user not
												// signed in
		}

		Log.i(LOG_TAG, "Tracking is called");

		// Create Location req and Location client
		setUpLocationClientIfNeeded();

		// Call send tracks to server service
		sendTracks();
		
		// saveTracks
//		saveTracks();

		return Service.START_NOT_STICKY;
	}

	/*private void saveTracks() {
		GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		
		// here convert date into long value
		date = calendar.getTime();
		
		track = new Track();
		//set currentLat, currentLng, date to Track Obj
		track.setLatitude(currentLat);
		track.setLongitude(currentLng);
		track.setTrackTime(date);
		
	}*/

	private void sendTracks() {
//		DatabaseHandler databaseHandler = new DatabaseHandler(context);
		// Get trackings
				/*List<Track> trackings = databaseHandler.getAllTracks();*/
		
		track = new Track();
		//set currentLat, currentLng, date to Track Obj
		track.setLatitude(currentLat);
		track.setLongitude(currentLng); 
//		track.setTrackTime(date);
				
				if (track != null && track.getLatitude() > 0 && track.getLongitude() > 0) { // TODO - hard coded to >1
					// Call send trackings to server service
					Intent sendTrackService = new Intent(this, SendTrackService.class);
					sendTrackService.putExtra("com.geeklabs.locationringer.track", track);
					this.startService(sendTrackService);			
				}
		
	}

	private void setUpLocationClientIfNeeded() {
		createLocReq();
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(getApplicationContext(), (ConnectionCallbacks) this, // ConnectionCallbacks
					this); // OnConnectionFailedListener
		}

	}

	private void createLocReq() {

		if (mLocationRequest == null) {
			// Create a new global location parameters object
			mLocationRequest = LocationRequest.create();
			// Set the update interval
			mLocationRequest.setInterval(LocationServices.UPDATE_INTERVAL_IN_MILLISECONDS);
			// Use high accuracy
			mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
			// Set the interval ceiling to one minute
			mLocationRequest.setFastestInterval(LocationServices.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
		}

	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			this.location = location;
			updateLocation(location);
		}

	}

	public Location getLocation() {
		return location;
	}

	public void updateLocation(Location location) {
		if (lastTrackedLat == 0.0) {
			lastTrackedLat = location.getLatitude();
		}
		if (lastTrackedLng == 0.0) {
			lastTrackedLng = location.getLongitude();
		}

		currentLat = location.getLatitude();
		currentLng = location.getLongitude();
	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {

	}
	public class UserLocationBinder extends Binder {
	    public TrackingService getService() {
	        return TrackingService.this;
	      }
	}

}
