package com.geeklabs.lreminder.service.reciever;

import com.geeklabs.locationrem.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.widget.Toast;

public class ProximityIntentReceiver extends BroadcastReceiver {
	private Context _context;

	@Override
	public void onReceive(Context arg0, Intent intent) {
		// String key = LocationManager.KEY_PROXIMITY_ENTERING;
		final String key = LocationManager.KEY_PROXIMITY_ENTERING;
		final Boolean entering = intent.getBooleanExtra(key, false);
		// Boolean entering = arg1.getBooleanExtra(key, false);
		String here = intent.getExtras().getString("alert");
		String happy = intent.getExtras().getString("type");

		NotificationManager notificationManager = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent pendingIntent = PendingIntent.getActivity(arg0, 0, intent, 0);

		Notification notification = createNotification();

		if (entering) {
			Toast.makeText(_context, "entering", Toast.LENGTH_SHORT).show();
			notification.setLatestEventInfo(arg0, "Entering Proximity!", "", pendingIntent);
		} else {
			Toast.makeText(_context, "exiting", Toast.LENGTH_SHORT).show();
			notification.setLatestEventInfo(arg0, "Existing Proximity!", "", pendingIntent);
		}

		notificationManager.notify((int) System.currentTimeMillis(), notification);

	}

	private Notification createNotification() {
		Notification notification = new Notification();

		notification.icon = R.drawable.ic_launcher;
		notification.when = System.currentTimeMillis();

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;

		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notification.defaults |= Notification.DEFAULT_LIGHTS;

		notification.ledARGB = Color.WHITE;
		notification.ledOnMS = 1500;
		notification.ledOffMS = 1500;

		return notification;
	}
	// make actions

}