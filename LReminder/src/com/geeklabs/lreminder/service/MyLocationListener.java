package com.geeklabs.lreminder.service;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

public class MyLocationListener implements android.location.LocationListener {
	private Context _context;

	public void onLocationChanged(Location location) {
		Toast.makeText(_context, "I was here", Toast.LENGTH_LONG).show();
	}

	public void onProviderDisabled(String s) {
	}

	public void onProviderEnabled(String s) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}
}
