package com.geeklabs.lreminder.communication.post.task;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.geeklabs.lreminder.communication.AbstractHttpPostTask;
import com.geeklabs.lreminder.domain.Track;
import com.geeklabs.lreminder.preferences.AuthPreferences;
import com.geeklabs.lreminder.util.RequestUrl;
import com.geeklabs.lreminder.util.ResponseStatus;

public class SendMyLocationTask extends AbstractHttpPostTask {

	private final Context context;
	private final AuthPreferences authPreferences;
	private Track track;
	
	public SendMyLocationTask(Context context, ProgressDialog progressDialog, Track track) {
		super(progressDialog, context);
		this.context = context;
		authPreferences = new AuthPreferences(context);
		this.track = track;
	}

	@Override
	protected String getRequestJSON() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(track);
		} catch (Exception e) {
			throw new IllegalStateException(
					"problem occured json parsing while sending tracks");
		} 
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SEND_TRACKS.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected void showMessageOnUI(String string) {
		// no need to show any message for back ground process
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		//TODO
		ObjectMapper mapper = new ObjectMapper();
		try {
			ResponseStatus status = mapper.readValue(jsonResponse.toString(),
					ResponseStatus.class);
			if ("success".equals(status.getStatus())) {
				Toast.makeText(context, "Success from server response", Toast.LENGTH_SHORT).show();
			}
			}
	catch (JsonParseException e) {
		e.printStackTrace();
	} catch (JsonMappingException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}

}
}
