package com.geeklabs.lreminder.util;
public final class RequestUrl {

	private RequestUrl() {}
	
	public static final String VALIDATE_TOKEN_REQ= "http://192.168.1.3:9998/user/signin";
	public static final String SIGNOUT_REQ = "http://192.168.1.3:9998/user/signout/{id}";
	public static final String SEND_TRACKS = "http://192.168.1.3:9998/track/saveTracks/{id}";
	public static final String GET_LOCATION = "http://192.168.1.3:9998/track/getLocation/{number}";
}