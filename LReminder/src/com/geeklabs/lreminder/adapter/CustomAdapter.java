package com.geeklabs.lreminder.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.geeklabs.locationrem.R;
import com.geeklabs.lreminder.sqlite.Reminder;

public class CustomAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<String> rems = new ArrayList<String>();
	private Reminder reminder;

	public CustomAdapter(Activity context) {
		mInflater = LayoutInflater.from(context);

	}

	@Override
	public int getCount() {
		return rems.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder vh;
		vh = new ViewHolder();

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.custom_row, parent, false);
			// inflate custom layout
			vh.tv1 = (TextView) convertView.findViewById(R.id.reminder_description_lable);
			vh.tv2 = (TextView) convertView.findViewById(R.id.reminder_title_lable);
			vh.tv3 = (TextView) convertView.findViewById(R.id.radius_lable);
			vh.checkBox = (CheckBox) convertView.findViewById(R.id.check_box);

		} else {
			convertView.setTag(vh);
		}
		// set specific title and description
		vh.tv1.setText(reminder.getTitle());
		vh.tv2.setText(reminder.getDesc());
		vh.tv3.setText("/n Radius:" + reminder.getDist());

		return convertView;
	}

	class ViewHolder {
		TextView tv1, tv2, tv3;
		CheckBox checkBox;
	}

}