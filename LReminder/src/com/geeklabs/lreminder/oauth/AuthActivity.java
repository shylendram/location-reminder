package com.geeklabs.lreminder.oauth;

import java.io.IOException;

import net.frakbot.accounts.chooser.AccountChooser;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.geeklabs.lreminder.activity.MainActivity;
import com.geeklabs.lreminder.communication.post.task.RegisterUserTask;
import com.geeklabs.lreminder.domain.User;
import com.geeklabs.lreminder.preferences.AuthPreferences;
import com.geeklabs.lreminder.service.TrackingService;

public class AuthActivity extends Activity {

	private static final int AUTHORIZATION_CODE = 1993;
	private static final int ACCOUNT_CODE = 1601;

	private AuthPreferences authPreferences;
	private AccountManager accountManager;
	private TrackingService trackingService;
	/*private MyLocationListener locationListenerImpl;
	private LocationManager locationManager;
	private Location lastKnownLocation;*/
	private Account userAccount;

	private ServiceConnection mConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder binder) {
			trackingService = ((TrackingService.UserLocationBinder) binder)
					.getService();
		}

		public void onServiceDisconnected(ComponentName className) {
			trackingService = null;
		}
	};
	private Intent intent;

	@Override
	protected void onResume() {
		super.onResume();
		bindService(new Intent(this, TrackingService.class), mConnection,
				Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unbindService(mConnection);
	}

	/**
	 * change this depending on the scope needed for the things you do in
	 * doCoolAuthenticatedStuff()
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		accountManager = AccountManager.get(this);
		authPreferences = new AuthPreferences(this);
		/*locationListenerImpl = new MyLocationListener();
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
				0, locationListenerImpl);
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerImpl);
		if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		} else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
*/
		// invalidate old tokens which might be cached. we want a fresh
		// one, which is guaranteed to work
		invalidateToken();

		chooseAccount();

	}

	private void chooseAccount() {
		// using https://github.com/frakbot/Android-AccountChooser for
		// compatibility with older devices
		Intent intent = AccountChooser.newChooseAccountIntent(null, null,
				new String[] { "com.google" }, true, null, null, null, null,
				getApplicationContext());

		startActivityForResult(intent, ACCOUNT_CODE);
	}

	private void requestToken() {
		String user = authPreferences.getUserAccount();
		for (Account account : accountManager.getAccountsByType("com.google")) {
			if (account.name.equals(user)) {
				userAccount = account;
				break;
			}
		}
		accountManager.getAuthToken(userAccount, "oauth2:"
				+ OAuthUserCredStore.SCOPE, null, this, new OnTokenAcquired(),
				null);

	}

	/**
	 * call this method if your token expired, or you want to request a new
	 * token for whatever reason. call requestToken() again afterwards in order
	 * to get a new token.
	 */
	private void invalidateToken() {
		AccountManager accountManager = AccountManager.get(this);
		accountManager.invalidateAuthToken("com.google",
				authPreferences.getAccessToken());

		authPreferences.setAccessToken(null);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			if (requestCode == AUTHORIZATION_CODE) {
				requestToken();
			} else if (requestCode == ACCOUNT_CODE) {
				String accountName = data
						.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
				authPreferences.setUserAccount(accountName);

				// invalidate old tokens which might be cached. we want a fresh
				// one, which is guaranteed to work
				invalidateToken();

				requestToken();
			}
		} else if (resultCode == RESULT_CANCELED) {
			Intent i = new Intent(this, MainActivity.class);
			this.startActivity(i);
		}
	}
	private String requestNewToken() {
		AccountManagerFuture<Bundle> accountManagerFuture = accountManager.getAuthToken(userAccount,
						"oauth2:" + OAuthUserCredStore.SCOPE, null, this,
						new OnNewTokenAcquired(), null);
		String newAuthToken = null;
		try {
			Bundle bundle = accountManagerFuture.getResult();
			newAuthToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
		} catch (OperationCanceledException e) {
			e.printStackTrace();
			// If user cancels operation, send back to sign in page
			Intent i = new Intent(AuthActivity.this, MainActivity.class);
			AuthActivity.this.startActivity(i);
		} catch (AuthenticatorException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return newAuthToken;

	}

	private class OnTokenAcquired implements AccountManagerCallback<Bundle> {

		@Override
		public void run(AccountManagerFuture<Bundle> result) {
			try {

				Bundle bundle = result.getResult();

				Intent launch = (Intent) bundle.get(AccountManager.KEY_INTENT);
				if (launch != null) {
					startActivityForResult(launch, AUTHORIZATION_CODE);
				} else {
					String token = bundle
							.getString(AccountManager.KEY_AUTHTOKEN);
					authPreferences.setAccessToken(token);
					invalidateToken();
					AsyncTask<Void, String, String> asyncTask = new AsyncTask<Void, String, String>() {

						@Override
						protected String doInBackground(Void... params) {
							return requestNewToken();
						}

					};
					asyncTask.execute();
					String newToken = asyncTask.get();
					// Register new user
					registerAndValidateUser(newToken);
				}

			} catch (OperationCanceledException canceledException) {
				// If user cancels operation, send back to sign in page
				Intent i = new Intent(AuthActivity.this, MainActivity.class);
				AuthActivity.this.startActivity(i);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

		}
	}

	private void registerAndValidateUser(String accessToken) {
		final ProgressDialog signingAPKProgressDialog = new ProgressDialog(AuthActivity.this);
		signingAPKProgressDialog.setMessage("User authentication is in progress...");
		Log.i("User authentication", "User authentication is in progress");
		signingAPKProgressDialog.setCancelable(false);

		// Job required for validation, registration will be taken care in
		User user = new User();
		user.setAccessToken(accessToken);
		
		/*
		
		Track firstTrack = new Track();
		firstTrack.setTrackTime(new Date());
		// TODO remove getting location from using tracking Service
		if (trackingService != null && trackingService.getLocation() != null) {
			Location location = trackingService.getLocation();
			firstTrack.setLatitude(location.getLatitude());
			firstTrack.setLongitude(location.getLongitude());
		} else if (lastKnownLocation != null) {
			firstTrack.setLatitude(lastKnownLocation.getLatitude());
			firstTrack.setLongitude(lastKnownLocation.getLongitude());
		} else {
			lastKnownLocation = locationListenerImpl.getCurrentLocation(getApplicationContext());
			if (lastKnownLocation != null) {
				firstTrack.setLatitude(lastKnownLocation.getLatitude());
				firstTrack.setLongitude(lastKnownLocation.getLongitude());
			}
		}
		// remove location updates
		locationManager.removeUpdates(locationListenerImpl);
*/
		RegisterUserTask validateUserRunnable = new RegisterUserTask(AuthActivity.this, signingAPKProgressDialog, user);
		validateUserRunnable.execute();
	}
}