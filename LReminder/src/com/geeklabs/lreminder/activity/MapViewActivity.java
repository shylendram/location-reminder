package com.geeklabs.lreminder.activity;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.Toast;

import com.geeklabs.locationrem.R;
import com.geeklabs.lreminder.util.MapUtil;

public class MapViewActivity extends FragmentActivity implements LocationListener {

	protected GoogleMap googleMap;
	private double userCurrentLatitude, userCurrentLongitude;
	private LocationManager locationManager;
	private String provider;
	private static final String TAG = "DisplayGPSInfoActivity";
	final Context context = this;

	NotificationCompat.Builder builder;
	NotificationManager nm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_view);

		// to show notification
		builder = new NotificationCompat.Builder(this);
		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		this.locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		// Choosing the best criteria depending on what is available.
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);
		// provider = LocationManager.GPS_PROVIDER; // We want to use the GPS

		// Initialize the location fields
		// Location location = locationManager.getLastKnownLocation(provider);

		locationManager.requestLocationUpdates(provider, 1000, 0, this);

		// Check is Wi-Fi available
		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		if (!wifi.isWifiEnabled()) {
			showAlertDailog(this, "Improve your location by enabling WiFi", "Enable Wi-Fi", NetworkType.WIFI);
			Log.i("WiFi", "WiFi is disabled");
			// Make sure user device has GPS enable
			if (!isGPSAvailable()) {
				showAlertDailog(this, "Improve your location by enabling GPS", "Enable GPS", NetworkType.GPS);
				Log.i("GPS", "GPS is disabled");

			}
		}

		else {
			// Check is network available
			checkNetworkAvailable(android.os.Process.myPid());

			// create Google map instance
			initGoogleMap();

			// To keep screen on until app is closed
			keepScreenActive();

			// listen when long press on map
			listenOnMapLongPress();

		}
	}

	private void initGoogleMap() {
		// confirm that we have not already instantiated the gmap
		if (googleMap == null) {
			android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
			googleMap = ((SupportMapFragment) fragmentManager.findFragmentById(R.id.map)).getMap();
			Log.i("Google map", "Successfully Googlemap is opened");
			// Check if we were successful in obtaining the map.
			if (googleMap != null) {
				// show toast for Long tap on map
				Toast.makeText(this, "Long tap on map to write your reminder", Toast.LENGTH_LONG).show();
				// It shows current location marker on map
				googleMap.setMyLocationEnabled(true);
			}
		}
	}

	// To keep screen on until app is closed
	private void keepScreenActive() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.v(TAG, "Resuming");
		locationManager.requestLocationUpdates(provider, 1000, 0, this);
	}

	@Override
	public void onProviderDisabled(String arg0) {

	}

	@Override
	public void onProviderEnabled(String arg0) {

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			userCurrentLatitude = location.getLatitude();
			userCurrentLongitude = location.getLongitude();
			Toast.makeText(this, "you are at: " + userCurrentLatitude + "," + userCurrentLongitude, Toast.LENGTH_LONG).show();

			// TODO check current lat lang with DB lat lang and show alert when
			// matches

			if (userCurrentLatitude == 18.6755221 && userCurrentLongitude == 78.1011562) {
//				showAlert();
			}
		}
	}

	public void showAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MapViewActivity.this);

		// set title
		alertDialogBuilder.setTitle("Reminder Alert");

		// set dialog message
		alertDialogBuilder.setMessage("You are near your point of interest. Do you want to open it now").setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked
						Intent intent = new Intent(MapViewActivity.this, NotificationReceiverActivity.class);
						startActivity(intent);
					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// to vibrate mobile
		Vibrator vibrator;
		vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(500);

		// to get sound
		final MediaPlayer mp = MediaPlayer.create(MapViewActivity.this, R.raw.alert);
		mp.start();

		// to show notification
		showNotification();

		// show it
		alertDialog.show();
	}

	private void showNotification() {
		Intent intent = new Intent(MapViewActivity.this, NotificationReceiverActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(MapViewActivity.this, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);

		builder.setContentTitle("Reminder Alert").setContentText("You want to do some thing here..Click here to open it now")
				.setTicker("you have a reminder alert").setSmallIcon(R.drawable.ic_launcher).setWhen(System.currentTimeMillis())
				.setContentIntent(pendingIntent);
		nm.notify(1, builder.build());
	}

	// To show alerts for GPS and Network services
	@SuppressWarnings("deprecation")
	private void showAlertDailog(MapViewActivity mapActivity, String message, String buttonLabel, final NetworkType networkType) {
		AlertDialog builder = new AlertDialog.Builder(this).create();

		builder.setMessage(message);

		builder.setButton(buttonLabel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				if (NetworkType.GPS == networkType) {
					MapUtil.showGpsOptions(MapViewActivity.this);
					Log.i("Gps settings", "Gps settings successfully shown");
					initGoogleMap();
				} else {
					MapUtil.showWifiOptions(MapViewActivity.this);
					Log.i("Wifi settings", "Wifi settings successfully shown");
					initGoogleMap();
				}
			}
		});

		builder.setButton2("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				return;
			}
		});

		builder.show();
	}

	enum NetworkType {
		GPS, WIFI, MOBILE
	}

	private void listenOnMapLongPress() {

		googleMap.setOnMapLongClickListener(new OnMapLongClickListener() {

			public void onMapLongClick(final LatLng latLng) {
				// Animating to the touched position
				googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

				Log.i("Long click", "Successfully shown reminder save form when long click on map");

				double latitude = latLng.latitude;
				double longitude = latLng.longitude;

				Toast.makeText(getApplicationContext(),
						"Long clicked on " + "Latitute:" + latitude + "," + "Longitude:" + longitude + " Successfully", Toast.LENGTH_LONG)
						.show();

				Intent intent = new Intent(getApplicationContext(), ReminderSaveActivity.class);
				intent.putExtra("lat", latitude);
				intent.putExtra("lang", longitude);
				startActivity(intent);
			}
		});

	}

	private boolean checkNetworkAvailable(final int processId) {
		ConnectivityManager conMgr = getConnManager();
		boolean isMoblieDataEnabled = conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
		boolean isWifiEnabled = conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
		if (!isMoblieDataEnabled && !isWifiEnabled && googleMap == null) {

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
			// set title
			alertDialogBuilder.setTitle("Warning");
			alertDialogBuilder.setMessage("Check your network connection and try again");
			// set dialog message
			alertDialogBuilder.setCancelable(false).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					finish();
					moveTaskToBack(true);
				}
			});
			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			return false;
		}
		return true;
	}

	private ConnectivityManager getConnManager() {
		ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		return conMgr;
	}

	// To check whether GPS,Wifi or Mobile data are available or not
	private boolean isGPSAvailable() {
		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	@Override
	public void onBackPressed() {

		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map_view, menu);
		return true;
	}

}
