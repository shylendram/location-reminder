package com.geeklabs.lreminder.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.geeklabs.locationrem.R;

public class NormalActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_page);

		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// go to next screen
				Intent intent = new Intent(getApplicationContext(), SelectOptionActivity.class);
				startActivity(intent);
			}
		});

	}

}
