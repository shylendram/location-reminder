package com.geeklabs.lreminder.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.geeklabs.locationrem.R;
import com.geeklabs.lreminder.alert.ProximityIntentReceiver;
import com.geeklabs.lreminder.sqlite.DatabaseHandler;
import com.geeklabs.lreminder.sqlite.Reminder;
import com.google.android.maps.OverlayItem;

public class ReminderSaveActivity extends Activity implements AdapterView.OnItemSelectedListener {

	private EditText editText1, editText2;
	private String title, desc, dist;
	private double reminderLat, reminderLang;
	private int distance;
	private Spinner spinner;
	private LocationManager locationManager;

	private static final long MINIMUM_DISTANCECHANGE_FOR_UPDATE = 1; // in
	// Meters
	private static final long MINIMUM_TIME_BETWEEN_UPDATE = 1000; // in
	// Milliseconds
	private static int POINT_RADIUS = 100; // in Meters
	private static final long PROX_ALERT_EXPIRATION = -1;

	private static final String PROX_ALERT_INTENT = "com.geeklabs.locationrem.ProximityAlert";

	// GPSTracker class
	GPSTracker gps;
	DatabaseHandler db = new DatabaseHandler(this);
	Integer[] radius = { 100, 500, 1000, 2000, 3000, 5000 };
	private Reminder reminder;
	ArrayList<Location> arrayList = new ArrayList<Location>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reminder_save_form);

		locationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);

		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MINIMUM_TIME_BETWEEN_UPDATE,
				MINIMUM_DISTANCECHANGE_FOR_UPDATE, new MyLocationListener());

		// get lat long when long tap from main Activity
		Intent intent = getIntent();
		reminderLat = intent.getDoubleExtra("lat", 0.0);
		reminderLang = intent.getDoubleExtra("lang", 0.0);

		editText1 = (EditText) findViewById(R.id.editText1);
		editText2 = (EditText) findViewById(R.id.editText2);
		spinner = (Spinner) findViewById(R.id.spinner1);

		reminder = (Reminder) getIntent().getSerializableExtra("reminder");
		if (reminder != null) {
			editText1.setText(reminder.getTitle());
			editText2.setText(reminder.getDesc());
			spinner.setSelection(POINT_RADIUS);
		}

		spinner.setOnItemSelectedListener(this);

		// Creating the ArrayAdapter instance having the country list
		ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, radius);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Setting the ArrayAdapter data on the Spinner
		spinner.setAdapter(adapter);

		findViewById(R.id.saveButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// get details from edit text's
				title = editText1.getText().toString();
				desc = editText2.getText().toString();
				dist = spinner.getSelectedItem().toString();

				try {
					distance = Integer.parseInt(dist);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}

				// store data in sqlite DB
				db.addReminder(new Reminder(title, desc, distance, reminderLat, reminderLang));

				// reading reminders from DB
				Log.d("Reading: ", "Reading all reminders..");
				List<Reminder> reminders = db.getAllReminders();

				for (Reminder rems : reminders) {
					String log = "Id: " + rems.getId() + " ,Title: " + rems.getTitle() + " ,Description: " + rems.getDesc() + ", Distance:"
							+ rems.getDist() + ", Latitude:" + rems.getReminderLat() + ", Longitude:" + rems.getReminderLang();
					// Writing reminders to log
					Log.d("Reminders: ", log);
				}

				Toast.makeText(getApplicationContext(), "Reminder is saved", Toast.LENGTH_SHORT).show();

				// save proximity alert point
				saveProximityAlertPoint();

				// when press back make all edit texts null
				editText1.setText("");
				editText2.setText("");

				startActivity(new Intent(getApplicationContext(), RemindersViewActivity.class));

				// Get location in background and update
				gps = new GPSTracker(ReminderSaveActivity.this);

				// check if GPS enabled
				if (gps.canGetLocation()) {

					double latitude = gps.getLatitude();
					double longitude = gps.getLongitude();

					// \n is for new line
					Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude,
							Toast.LENGTH_LONG).show();
				} else {
					// Ask user to enable GPS/network in settings
					gps.showSettingsAlert();
				}

			}

			private void saveProximityAlertPoint() {
				Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				if (location == null) {
					Toast.makeText(getApplicationContext(), "No last known location. Location is null", Toast.LENGTH_LONG).show();
					return;
				}
				addProximityAlert(location.getLatitude(), location.getLongitude());
			}

			private void addProximityAlert(double latitude, double longitude) {

				Intent intent = new Intent(PROX_ALERT_INTENT);
				PendingIntent proximityIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);

				locationManager.addProximityAlert(latitude, // the latitude of
															// the
															// central point of
															// the
															// alert region
						longitude, // the longitude of the central point of the
									// alert
									// region
						POINT_RADIUS, // the radius of the central point of the
										// alert
										// region, in meters
						PROX_ALERT_EXPIRATION, // time for this proximity alert,
												// in
												// milliseconds, or -1 to
												// indicate no
												// expiration
						proximityIntent // will be used to generate an Intent to
										// fire
										// when entry to or exit from the alert
										// region
										// is detected
						);

				IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
				registerReceiver(new ProximityIntentReceiver(), filter);
			}
		});

	}

	// Performing action onItemSelected and onNothing selected
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {

		if (position == 0) {
		} else {
			Toast.makeText(getApplicationContext(), String.valueOf(radius[position]), Toast.LENGTH_LONG).show();
			POINT_RADIUS = radius[position];
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	class MyLocationListener implements LocationListener {
		@Override
		public void onLocationChanged(Location location) {
			ArrayList<OverlayItem> allSavedLocations = db.getAllSavedLocations();
			for (OverlayItem item : allSavedLocations) {
				if (location.equals(item)) {
					// TODO
					Toast.makeText(ReminderSaveActivity.this, "Got alert", Toast.LENGTH_LONG).show();
				}
			}
			/*
			 * float distance = location.distanceTo(pointLocation);
			 * Toast.makeText(ReminderSaveActivity.this, "Distance from Point:"
			 * + distance, Toast.LENGTH_LONG).show();
			 */
		}

		/*
		 * private Location retrievelocationFromDB() { double[] locations;
		 * Location location = new Location("POINT_LOCATION");
		 * location.setLatitude(reminderLat);
		 * location.setLongitude(reminderLang); arrayList.add(location); return
		 * location; }
		 */

		@Override
		public void onStatusChanged(String s, int i, Bundle b) {
		}

		@Override
		public void onProviderDisabled(String s) {
		}

		@Override
		public void onProviderEnabled(String s) {
		}
	}
}
