package com.geeklabs.lreminder.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.geeklabs.locationrem.R;
import com.geeklabs.lreminder.oauth.AuthActivity;
import com.geeklabs.lreminder.util.NetworkService;

public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
			findViewById(R.id.sign_in_button).setOnClickListener(
					new OnClickListener() {
						@Override
						public void onClick(View v) {
							// Check for n/w connection
							if (!NetworkService
									.isNetWorkAvailable(MainActivity.this)) {
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
										MainActivity.this);
								// set title
								alertDialogBuilder.setTitle("Warning");
								alertDialogBuilder
										.setMessage("Check your network connection and try again.");
								// set dialog message
								alertDialogBuilder
										.setCancelable(false)
										.setNegativeButton(
												"Ok",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
													}
												});
								// create alert dialog
								AlertDialog alertDialog = alertDialogBuilder
										.create();

								// show it
								alertDialog.show();
							} else {
								Intent i = new Intent(getApplicationContext(), AuthActivity.class);
								startActivity(i);
							}
						}
					});
	}
}
